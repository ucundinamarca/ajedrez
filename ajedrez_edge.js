/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1026', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_1',
                                type: 'image',
                                rect: ['0px', '0px', '1026px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'msg',
                                type: 'group',
                                rect: ['250', '180', '526', '280', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_2',
                                    type: 'image',
                                    rect: ['0px', '0px', '526px', '280px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                                },
                                {
                                    id: 'ok',
                                    type: 'image',
                                    rect: ['211px', '232px', '104px', '35px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"ok.png",'0px','0px']
                                },
                                {
                                    id: 'icon1',
                                    type: 'ellipse',
                                    rect: ['245px', '205px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'icon2',
                                    type: 'ellipse',
                                    rect: ['270px', '205px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'page_1',
                                    type: 'group',
                                    rect: ['44', '76px', '440', '87', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'TextCopy',
                                        type: 'text',
                                        rect: ['0px', '-22px', '440px', '87px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: justify;\">​Este primer juego busca medir la apropiación e impacto de los “Valores del servicio público” en la cotidianidad de los servidores públicos. Con el fin de realizar una medición de la implementación del Código y las actividades contenidas en la caja de herramientas, se elaboró un test de percepción, con el cual se analizará la apropiación del Código de Integridad mostrando la percepción que tiene el servidor público de sus compañeros, de sus jefes, de él mismo, de la normativa que regula y vigila la integridad de su entidad en general.</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'page_2',
                                    type: 'group',
                                    rect: ['44', '89', '440', '87', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'Text',
                                        type: 'text',
                                        rect: ['0px', '0px', '440px', '87px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: justify;\">​Responda las preguntas que irán apareciendo en el juego. Usted cuenta con 30 segundos para contestarlas todas. Una vez responda la pregunta, el sistema moverá la ficha por usted. Al finalizar el juego obtendrá una medalla y un puntaje acorde a su desempeño.&nbsp;&nbsp;</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    },
                                    {
                                        id: 'Text2Copy6',
                                        type: 'text',
                                        rect: ['-30px', '-45px', '499px', '28px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​INSTRUCCIONES DEL JUEGO</p>",
                                        align: "center",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", ""],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                }]
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '0', '1026', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_1Copy',
                                type: 'image',
                                rect: ['0px', '0px', '1026px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'grupo',
                                type: 'group',
                                rect: ['249', '120', '526', '400', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_4',
                                    type: 'image',
                                    rect: ['0px', '0px', '526px', '400px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                                },
                                {
                                    id: 'a6',
                                    type: 'image',
                                    rect: ['366px', '248px', '128px', '128px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a6.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a5',
                                    type: 'image',
                                    rect: ['199px', '248px', '129px', '128px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a5.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a4',
                                    type: 'image',
                                    rect: ['34px', '248px', '128px', '128px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a4.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a3',
                                    type: 'image',
                                    rect: ['366px', '96px', '128px', '128px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a3.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a2',
                                    type: 'image',
                                    rect: ['199px', '96px', '129px', '129px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a2.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a1',
                                    type: 'image',
                                    rect: ['34px', '97px', '128px', '128px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"a1.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'Text2Copy9',
                                    type: 'text',
                                    rect: ['15px', '34px', '499px', '28px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​ELIGE UN JUGADOR</p>",
                                    align: "center",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", ""],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['-7px', '0px', '1026', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_1Copy2',
                                type: 'image',
                                rect: ['0px', '0px', '1026px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_11',
                                type: 'image',
                                rect: ['23px', '24px', '592px', '592px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                            },
                            {
                                id: 'tablero',
                                symbolName: 'tablero',
                                type: 'rect',
                                rect: ['53', '98', '534', '444', 'auto', 'auto']
                            },
                            {
                                id: 'Recurso_14',
                                type: 'image',
                                rect: ['626px', '20px', '184px', '48px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_15',
                                type: 'image',
                                rect: ['825px', '22px', '184px', '308px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            },
                            {
                                id: 'q',
                                type: 'group',
                                rect: ['635', '338', '374', '278', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_16',
                                    type: 'image',
                                    rect: ['0px', '0px', '374px', '278px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                                },
                                {
                                    id: 'QText',
                                    type: 'text',
                                    rect: ['19px', '18px', '337px', '190px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'a6_',
                                type: 'image',
                                rect: ['653px', '78px', '129px', '129px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a6.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a5_',
                                type: 'image',
                                rect: ['653px', '78px', '130px', '129px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a5.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a4_',
                                type: 'image',
                                rect: ['653px', '78px', '129px', '129px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a4.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a3_',
                                type: 'image',
                                rect: ['653px', '78px', '129px', '129px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a3.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a2_',
                                type: 'image',
                                rect: ['653px', '78px', '130px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a2.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a1_',
                                type: 'image',
                                rect: ['653px', '78px', '129px', '129px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"a1.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'robot',
                                type: 'image',
                                rect: ['654px', '79px', '128px', '128px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"robot.png",'0px','0px']
                            },
                            {
                                id: 'Text2',
                                type: 'text',
                                rect: ['626px', '36px', '184px', '28px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​JUGADOR</p>",
                                align: "center",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", ""],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2Copy',
                                type: 'text',
                                rect: ['873px', '39px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​PUNTAJE</p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2Copy2',
                                type: 'text',
                                rect: ['871px', '126px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​PREGUNTA</p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2Copy5',
                                type: 'text',
                                rect: ['929px', '273px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 10px;\">Seg</span></p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2Copy3',
                                type: 'text',
                                rect: ['881px', '228px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​TIEMPO</p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'r',
                                type: 'text',
                                rect: ['875px', '269px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​30</p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [25, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Group8',
                                type: 'group',
                                rect: ['855', '167', '105', '29', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Text2Copy8',
                                    type: 'text',
                                    rect: ['51px', '3px', 'auto', 'auto', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 10px;\">DE</span></p>",
                                    align: "left",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'n',
                                    type: 'text',
                                    rect: ['-7px', '0px', '52px', '29px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​1</p>",
                                    align: "center",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [25, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", ""],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Text2Copy7',
                                    type: 'text',
                                    rect: ['77px', '0px', 'auto', 'auto', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​16</p>",
                                    align: "center",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [25, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'qm',
                                type: 'text',
                                rect: ['651px', '352px', '341px', '249px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "justify",
                                font: ['Arial, Helvetica, sans-serif', [11, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2Copy10',
                                type: 'text',
                                rect: ['918px', '80px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 10px;\">X</span></p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'm',
                                type: 'text',
                                rect: ['936px', '77px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​1</p>",
                                align: "left",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [25, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'maney',
                                type: 'image',
                                rect: ['875px', '72px', '30px', '30px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"maney.svg",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage4',
                            type: 'group',
                            rect: ['0', '0', '1026', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_1Copy3',
                                type: 'image',
                                rect: ['0px', '0px', '1026px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_19',
                                type: 'image',
                                rect: ['264px', '119px', '526px', '360px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            },
                            {
                                id: 'okCopy2',
                                type: 'image',
                                rect: ['461px', '406px', '104px', '35px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ok.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                            [ "eid80", "trigger", 0, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${tablero}', [] ] ]
                    ]
                }
            },
            "tablero": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['269px', '223px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-1',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            rect: ['224px', '180px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-2',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-1',
                            rect: ['0px', '403px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-3',
                            rect: ['315px', '314px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-4',
                            rect: ['268px', '135px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-5',
                            rect: ['360px', '403px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        },
                        {
                            rect: ['358px', '135px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-3',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            rect: ['268px', '135px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-4',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            rect: ['357px', '45px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-5',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            rect: ['403px', '269px', '40px', '40px', 'auto', 'auto'],
                            id: 'f2-6',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/f2-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-6',
                            rect: ['313px', '89px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'f1-2',
                            rect: ['403px', '0px', '40px', '40px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/f1-1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '534px', '444px']
                        }
                    }
                },
                timeline: {
                    duration: 30000,
                    autoPlay: true,
                    labels: {
                        "p1": 250,
                        "p2": 1000,
                        "p3": 1750,
                        "p4": 2500,
                        "p5": 3250,
                        "p6": 4500,
                        "p7": 5250,
                        "p8": 6000,
                        "p9": 6750,
                        "p10": 7500,
                        "p11": 8250,
                        "p12": 9000,
                        "p13": 10000,
                        "p14": 11000,
                        "p15": 12000,
                        "p16": 13500,
                        "p17": 14500,
                        "p18": 15500,
                        "p19": 16500,
                        "p20": 17500,
                        "p21": 18500,
                        "p22": 19500,
                        "p23": 20500,
                        "p24": 22000,
                        "p25": 23000,
                        "p26": 24000,
                        "p27": 25000,
                        "p28": 26000,
                        "p29": 27000,
                        "p30": 28000,
                        "p31": 28750,
                        "p32": 30000
                    },
                    data: [
                        [
                            "eid98",
                            "left",
                            13500,
                            500,
                            "linear",
                            "${f2-2}",
                            '89px',
                            '134px'
                        ],
                        [
                            "eid114",
                            "left",
                            15500,
                            500,
                            "linear",
                            "${f2-2}",
                            '134px',
                            '179px'
                        ],
                        [
                            "eid129",
                            "left",
                            17500,
                            500,
                            "linear",
                            "${f2-2}",
                            '179px',
                            '224px'
                        ],
                        [
                            "eid157",
                            "opacity",
                            21000,
                            500,
                            "linear",
                            "${f2-2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid67",
                            "top",
                            7500,
                            500,
                            "linear",
                            "${f2-3}",
                            '0px',
                            '45px'
                        ],
                        [
                            "eid79",
                            "top",
                            9000,
                            500,
                            "linear",
                            "${f2-3}",
                            '45px',
                            '90px'
                        ],
                        [
                            "eid86",
                            "top",
                            11000,
                            500,
                            "linear",
                            "${f2-3}",
                            '90px',
                            '135px'
                        ],
                        [
                            "eid147",
                            "left",
                            19500,
                            500,
                            "linear",
                            "${f2-1}",
                            '44px',
                            '89px'
                        ],
                        [
                            "eid168",
                            "left",
                            22000,
                            500,
                            "linear",
                            "${f2-1}",
                            '89px',
                            '134px'
                        ],
                        [
                            "eid180",
                            "left",
                            24000,
                            500,
                            "linear",
                            "${f2-1}",
                            '134px',
                            '179px'
                        ],
                        [
                            "eid193",
                            "left",
                            26000,
                            500,
                            "linear",
                            "${f2-1}",
                            '179px',
                            '224px'
                        ],
                        [
                            "eid205",
                            "left",
                            28000,
                            500,
                            "linear",
                            "${f2-1}",
                            '224px',
                            '269px'
                        ],
                        [
                            "eid148",
                            "top",
                            19500,
                            500,
                            "linear",
                            "${f2-1}",
                            '1px',
                            '45px'
                        ],
                        [
                            "eid169",
                            "top",
                            22000,
                            500,
                            "linear",
                            "${f2-1}",
                            '45px',
                            '90px'
                        ],
                        [
                            "eid181",
                            "top",
                            24000,
                            500,
                            "linear",
                            "${f2-1}",
                            '90px',
                            '135px'
                        ],
                        [
                            "eid194",
                            "top",
                            26000,
                            500,
                            "linear",
                            "${f2-1}",
                            '135px',
                            '180px'
                        ],
                        [
                            "eid206",
                            "top",
                            28000,
                            500,
                            "linear",
                            "${f2-1}",
                            '180px',
                            '223px'
                        ],
                        [
                            "eid29",
                            "opacity",
                            3500,
                            250,
                            "linear",
                            "${f2-4}",
                            '1',
                            '0'
                        ],
                        [
                            "eid215",
                            "opacity",
                            29250,
                            500,
                            "linear",
                            "${f2-1}",
                            '1',
                            '0'
                        ],
                        [
                            "eid99",
                            "top",
                            13500,
                            500,
                            "linear",
                            "${f2-2}",
                            '45px',
                            '90px'
                        ],
                        [
                            "eid115",
                            "top",
                            15500,
                            500,
                            "linear",
                            "${f2-2}",
                            '90px',
                            '135px'
                        ],
                        [
                            "eid130",
                            "top",
                            17500,
                            500,
                            "linear",
                            "${f2-2}",
                            '135px',
                            '180px'
                        ],
                        [
                            "eid12",
                            "top",
                            1000,
                            500,
                            "linear",
                            "${f2-6}",
                            '134px',
                            '180px'
                        ],
                        [
                            "eid40",
                            "top",
                            4500,
                            500,
                            "linear",
                            "${f2-6}",
                            '180px',
                            '224px'
                        ],
                        [
                            "eid52",
                            "top",
                            6000,
                            500,
                            "linear",
                            "${f2-6}",
                            '224px',
                            '269px'
                        ],
                        [
                            "eid66",
                            "left",
                            7500,
                            500,
                            "linear",
                            "${f2-3}",
                            '224px',
                            '269px'
                        ],
                        [
                            "eid78",
                            "left",
                            9000,
                            500,
                            "linear",
                            "${f2-3}",
                            '269px',
                            '313px'
                        ],
                        [
                            "eid85",
                            "left",
                            11000,
                            500,
                            "linear",
                            "${f2-3}",
                            '313px',
                            '358px'
                        ],
                        [
                            "eid95",
                            "opacity",
                            12500,
                            500,
                            "linear",
                            "${f2-3}",
                            '1',
                            '0'
                        ],
                        [
                            "eid44",
                            "top",
                            5250,
                            500,
                            "linear",
                            "${f1-6}",
                            '360px',
                            '314px'
                        ],
                        [
                            "eid58",
                            "top",
                            6750,
                            500,
                            "linear",
                            "${f1-6}",
                            '314px',
                            '224px'
                        ],
                        [
                            "eid73",
                            "top",
                            8250,
                            500,
                            "linear",
                            "${f1-6}",
                            '224px',
                            '180px'
                        ],
                        [
                            "eid92",
                            "top",
                            12000,
                            1000,
                            "linear",
                            "${f1-6}",
                            '180px',
                            '89px'
                        ],
                        [
                            "eid200",
                            "top",
                            27000,
                            500,
                            "linear",
                            "${f1-6}",
                            '89px',
                            '45px'
                        ],
                        [
                            "eid142",
                            "top",
                            10000,
                            500,
                            "linear",
                            "${f1-3}",
                            '404px',
                            '358px'
                        ],
                        [
                            "eid175",
                            "top",
                            23000,
                            500,
                            "linear",
                            "${f1-3}",
                            '358px',
                            '314px'
                        ],
                        [
                            "eid188",
                            "top",
                            25000,
                            500,
                            "linear",
                            "${f1-3}",
                            '314px',
                            '269px'
                        ],
                        [
                            "eid212",
                            "top",
                            28750,
                            1000,
                            "linear",
                            "${f1-3}",
                            '269px',
                            '180px'
                        ],
                        [
                            "eid6",
                            "top",
                            250,
                            500,
                            "linear",
                            "${f1-2}",
                            '268px',
                            '224px'
                        ],
                        [
                            "eid16",
                            "top",
                            1750,
                            500,
                            "linear",
                            "${f1-2}",
                            '224px',
                            '180px'
                        ],
                        [
                            "eid26",
                            "top",
                            3250,
                            500,
                            "linear",
                            "${f1-2}",
                            '180px',
                            '90px'
                        ],
                        [
                            "eid31",
                            "top",
                            3750,
                            500,
                            "linear",
                            "${f1-2}",
                            '90px',
                            '0px'
                        ],
                        [
                            "eid5",
                            "left",
                            250,
                            500,
                            "linear",
                            "${f1-2}",
                            '137px',
                            '179px'
                        ],
                        [
                            "eid15",
                            "left",
                            1750,
                            500,
                            "linear",
                            "${f1-2}",
                            '179px',
                            '224px'
                        ],
                        [
                            "eid25",
                            "left",
                            3250,
                            500,
                            "linear",
                            "${f1-2}",
                            '224px',
                            '313px'
                        ],
                        [
                            "eid30",
                            "left",
                            3750,
                            500,
                            "linear",
                            "${f1-2}",
                            '313px',
                            '403px'
                        ],
                        [
                            "eid43",
                            "left",
                            5250,
                            500,
                            "linear",
                            "${f1-6}",
                            '494px',
                            '448px'
                        ],
                        [
                            "eid57",
                            "left",
                            6750,
                            500,
                            "linear",
                            "${f1-6}",
                            '448px',
                            '358px'
                        ],
                        [
                            "eid72",
                            "left",
                            8250,
                            500,
                            "linear",
                            "${f1-6}",
                            '358px',
                            '313px'
                        ],
                        [
                            "eid91",
                            "left",
                            12000,
                            1000,
                            "linear",
                            "${f1-6}",
                            '313px',
                            '403px'
                        ],
                        [
                            "eid199",
                            "left",
                            27000,
                            500,
                            "linear",
                            "${f1-6}",
                            '403px',
                            '357px'
                        ],
                        [
                            "eid19",
                            "left",
                            2500,
                            500,
                            "linear",
                            "${f2-4}",
                            '224px',
                            '268px'
                        ],
                        [
                            "eid163",
                            "left",
                            21500,
                            0,
                            "linear",
                            "${f2-4}",
                            '268px',
                            '268px'
                        ],
                        [
                            "eid141",
                            "left",
                            10000,
                            500,
                            "linear",
                            "${f1-3}",
                            '179px',
                            '135px'
                        ],
                        [
                            "eid174",
                            "left",
                            23000,
                            500,
                            "linear",
                            "${f1-3}",
                            '135px',
                            '179px'
                        ],
                        [
                            "eid187",
                            "left",
                            25000,
                            500,
                            "linear",
                            "${f1-3}",
                            '179px',
                            '224px'
                        ],
                        [
                            "eid211",
                            "left",
                            28750,
                            1000,
                            "linear",
                            "${f1-3}",
                            '224px',
                            '316px'
                        ],
                        [
                            "eid216",
                            "left",
                            29750,
                            0,
                            "linear",
                            "${f1-3}",
                            '316px',
                            '317px'
                        ],
                        [
                            "eid103",
                            "top",
                            14500,
                            500,
                            "linear",
                            "${f1-4}",
                            '358px',
                            '315px'
                        ],
                        [
                            "eid124",
                            "top",
                            16500,
                            500,
                            "linear",
                            "${f1-4}",
                            '315px',
                            '269px'
                        ],
                        [
                            "eid136",
                            "top",
                            18500,
                            500,
                            "linear",
                            "${f1-4}",
                            '269px',
                            '224px'
                        ],
                        [
                            "eid154",
                            "top",
                            20500,
                            1000,
                            "linear",
                            "${f1-4}",
                            '224px',
                            '135px'
                        ],
                        [
                            "eid20",
                            "top",
                            2500,
                            500,
                            "linear",
                            "${f2-4}",
                            '90px',
                            '135px'
                        ],
                        [
                            "eid161",
                            "top",
                            21500,
                            0,
                            "linear",
                            "${f2-4}",
                            '135px',
                            '135px'
                        ],
                        [
                            "eid11",
                            "left",
                            1000,
                            500,
                            "linear",
                            "${f2-6}",
                            '447px',
                            '403px'
                        ],
                        [
                            "eid39",
                            "left",
                            4500,
                            500,
                            "linear",
                            "${f2-6}",
                            '403px',
                            '358px'
                        ],
                        [
                            "eid51",
                            "left",
                            6000,
                            500,
                            "linear",
                            "${f2-6}",
                            '358px',
                            '403px'
                        ],
                        [
                            "eid102",
                            "left",
                            14500,
                            500,
                            "linear",
                            "${f1-4}",
                            '313px',
                            '268px'
                        ],
                        [
                            "eid123",
                            "left",
                            16500,
                            500,
                            "linear",
                            "${f1-4}",
                            '268px',
                            '223px'
                        ],
                        [
                            "eid135",
                            "left",
                            18500,
                            500,
                            "linear",
                            "${f1-4}",
                            '223px',
                            '179px'
                        ],
                        [
                            "eid153",
                            "left",
                            20500,
                            1000,
                            "linear",
                            "${f1-4}",
                            '179px',
                            '268px'
                        ],
                        [
                            "eid185",
                            "width",
                            25500,
                            0,
                            "linear",
                            "${f1-3}",
                            '40px',
                            '40px'
                        ],
                        [
                            "eid61",
                            "opacity",
                            7000,
                            250,
                            "linear",
                            "${f2-6}",
                            '1',
                            '0'
                        ],
                        [
                            "eid34",
                            "opacity",
                            4000,
                            250,
                            "linear",
                            "${f2-5}",
                            '1',
                            '0'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("ajedrez_edgeActions.js");
})("EDGE-18778299");
