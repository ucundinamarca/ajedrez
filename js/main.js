
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Damas Chinas",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic" },
    {url:"sonidos/coin.mp3",       name:"coin" }
];
var preguntas=[
    {
        q:"Me motiva servir a la sociedad y a mi país; tengo claro que mi trabajo es una vocación y no una forma rápida de resolver mis problemas económicos.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Un servidor público no interviene en los procesos de selección de su entidad para beneficiar a ninguna persona conocida, pues conoce que los cargos se adjudican por meritocracia.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Los servidores públicos han aprendido a no discriminar a sus compañeros por posiciones políticas, ni sexuales, de religión, edad, clase social, etc.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Cuando un servidor público tiene falencias en su trabajo, debe asumirlas con responsabilidad. No debe descargar su obligación en los demás, pues esto no genera un compromiso con la entidad y la sociedad.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Un servidor público debe ser selectivo a la hora de realizar su trabajo, afrontando las problemáticas que se presentan con soluciones fáciles. Cuando la labor sea difícil de resolver, debe ignorarla y no solucionarla.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Los servidores públicos son promotores de igualdad en su vida diaria, por lo tanto, brindan las mismas oportunidades a todas las personas.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Un servidor público no cede ante presiones e intereses externos en las labores de su cargo.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Cuando decidí ser servidor público, me comprometí a vivir profesional y personalmente los valores promulgados en la entidad donde laboro.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Como servidor público he aprendido que mi desempeño diario es la clave para construir la confianza que la sociedad necesita.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Un servidor público tiene claro que es referente de comportamiento frente a cualquier ciudadano, por lo cual debe esforzarse por resolver todos los problemas personales o laborales de forma transparente.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"En todas las entidades del Estado, la meritocracia se ha convertido en la práctica concurrente para la contratación de personal.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Un servidor público conoce la información clara, precisa y transparente antes de tomar una decisión.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"“La integridad consiste en la coherencia entre las declaraciones y las realizaciones…” y agrega “…La integridad es esencial para que sean eficientes las relaciones interpersonales, porque el engaño desfigura los mensajes que transmitimos, crea una niebla y ya no sabemos de qué estábamos hablando” (Downs, 1957). La integridad es una característica personal que, en el sector público, también se refiere al cumplimiento de la promesa que cada servidor público le hace al Estado y a la ciudadanía de cumplir a cabalidad su labor.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Ley, moral y cultura se entienden como sistemas reguladores del comportamiento o sistemas normativos en el marco de un grupo social. Son, entonces, formas de regulación jurídica, cultural (colectivo) y moral (individual). Cuando las leyes (colectivas) no están en armonía con mi moral (individual) no hay apropiación del sentido de lo público o se promueve la ilegalidad.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Actúo siempre con fundamento en la verdad, cumpliendo mis deberes con transparencia y rectitud, y siempre favoreciendo el interés general",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Soy consciente de la importancia de mi rol como servidor público y estoy en disposición permanente para comprender y resolver las necesidades de las personas con las que me relaciono en mis labores cotidianas, buscando siempre mejorar su bienestar",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Cumplo con los deberes, funciones y responsabilidades asignadas a mi cargo de la mejor manera posible, con atención, prontitud, destreza y eficiencia, para así optimizar el uso de los recursos del Estad",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"El director del área de Recursos Humanos de una entidad es designado como uno de los integrantes del comité de selección de personal para un nuevo proyecto. Uno de los postulados a la vacante de asistente técnico es amigo suyo, dado que es primo de su esposa; sin embargo, este dato no lo conocen los demás miembros del comité de selección y él decide callarlo. Con la situación descrita, usted está…:",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Siempre digo la verdad, incluso cuando cometo errores, porque es humano cometerlos, pero no es correcto esconderlos.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Apoyo y promuevo los espacios de participación para que los ciudadanos hagan parte de la toma de decisiones que los afecten, relacionadas con mi cargo o labor.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Uso recursos públicos para fines personales relacionados con mi familia, mis estudios y mis pasatiempos (esto incluye el tiempo de mi jornada laboral, los elementos y bienes asignados para cumplir con mi labor, entre otros).",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Es necesario establecer dentro de la Universidad de Cundinamarca un sistema guía que estructure el conjunto de conductas y disposiciones aceptables, transparentes, éticas, autorreguladas, autocontroladas, autónomas, independientes y democráticas.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Es importante consolidar la integridad como principal aspecto en la prevención de la corrupción y motor del cambio de los comportamientos de los servidores y la cultura de las entidades.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Es necesario que el Código Autonómico sea acogido, aplicado y promovido de manera responsable por todos y cada uno de los servidores y funcionarios de la Universidad de Cundinamarca, incluyendo tanto administrativos como personal docente, con el objeto de asegurar que el comportamiento de cada uno de ellos sea acorde a lo establecido en el ideario y compromiso ético de la institución.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Es importante que dentro de una organización social del conocimiento se dé el reconocimiento de los grupos de interés, como la comunidad universitaria, los organismos públicos, las asociaciones y las organizaciones.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    },
    {
        q:"Los lineamientos de buen gobierno contenidos en el Código Autonómico deben responder a los compromisos adquiridos por la Universidad.",
        r1:"Totalmente de acuerdo",
        r2:"De acuerdo",
        r3:"En desacuerdo",
        r4:"Totalmente en desacuerdo",
    }
];
index=0;
var tiempo="";
var contador=0;
var centesimas = 0;
var segundos = 0;
var minutos = 0;
var horas = 0;
function timer () {
	if (centesimas < 99) {
		centesimas++;
		if (centesimas < 10) { centesimas = "0"+centesimas }
	}
	if (centesimas == 99) {
		centesimas = -1;
	}
	if (centesimas == 0) {
		segundos ++;
		if (segundos < 10) { segundos = "0"+segundos }
	}
	if (segundos == 59) {
		segundos = -1;
	}
	if ( (centesimas == 0)&&(segundos == 0) ) {
		minutos++;
		if (minutos < 10) { minutos = "0"+minutos }
	}
	if (minutos == 59) {
		minutos = -1;
	}
	if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
		horas ++;
		if (horas < 10) { horas = "0"+horas }
	}
    tiempo = horas+" : "+minutos+" : "+segundos+" : "+centesimas;
}
function cronometro(time,f,f2,start){
    start();
    reloj= setInterval(function(){
        console.log(time);
        if(time<=0){
            clearInterval(reloj);
            f2();
        }
        let label="";
        if(time<10){
            label="0"+time;
        }else{
            label=time;
        }
        f(label);
        time=time-1;
    },1000)
}
var puntos=0;
function main(sym) {
preguntas = preguntas.sort(function(a,b) {return (Math.random()-0.5)});
udec = ivo.structure({
        created:function(){
           t=this;
           ivo(ST+"m").text("0");
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
                ivo(".ins").hide();
                ivo(ST+"page_1").show();
                ivo(ST+"robot").hide();
                ivo(ST+"preload").hide();
                t.events();
                t.animation();
                stage1.play();
           });
           Game_UdeC.info_moodle();
        },
        methods: {
            launch:function(){
                
                cronometro(30,
                    function(txt){
                        ivo(ST+"r").text(txt);

                    },
                    function(){
                        index+=1;
                        if(index<=16){
                            clearInterval(reloj);
                            ivo(ST+"n").text(index+1);
                            t.launch();
                        }else{
                            ivo(ST+"qm").html("");
                        }
                    },
                    function(){
                        ivo(ST+"qm").html(`
                            <div>${index+1}). ${preguntas[index].q}</div>
                            <ul>
                                <li>a. ${preguntas[index].r1}</li>
                                <li>b. ${preguntas[index].r2}</li>
                                <li>c. ${preguntas[index].r3}</li>
                                <li>d. ${preguntas[index].r4}</li>
                            </ul>
                        `);

                        ivo("ul li").on("click",function(){
                            clearInterval(reloj);
                            ivo(ST+"robot").hide();
                            preguntas[index].r=ivo(this).text().split(". ")[1];
                            index+=1;
                            puntos+=1;
                            contador+=1;
                            if(index<16){
                                t.launch();
                                ivo(ST+"n").text(index+1);
                                ivo(ST+"m").text(puntos);
                                ivo.play("coin");
                                setTimeout(function(){
                                    
                                    contador+=1;
                                    sym.getSymbol("tablero").play("p"+contador);
                                    ivo(ST+"robot").show();
                                    ivo(ST+"Text2 p").text("CPU");
                                    setTimeout(function(){
                                       
                                        ivo(ST+"Text2 p").text("JUGADOR");
                                        ivo(ST+"robot").hide();
                                    },1500);
                                },1500);
                            }else{
                                preguntas.push({q:"Tiempo utilizado en la prueba: (H:M:S:M) "+tiempo,r:"Tiempo",r1:"",r2:"",r3:"",r4:""});
                                Game_UdeC.data.quiz = preguntas;
                   
                                Game_UdeC.data.game = "Damas Chinas";
                                Game_UdeC.data.url_game = 'https://virtual.ucundinamarca.edu.co/red/Otros/games/ajedrez/ajedrez.html';
                                Game_UdeC.save();
                                
                                Swal.fire({
                                    title: '<strong>Retroalimentación</strong>',imageUrl: './images/home.png',
                                    imageHeight: 100,
                                    imageAlt: 'A tall image',
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    html:
                                      'Recuerde que la labor del servicio público debe ser organizada y debe responder a lo estipulado por la ley y los reglamentos vigentes, pues solo así se responderá manera adecuada a las necesidades colectivas que se derivan de su quehacer.' ,
                                    
                                  }); 
                                  ivo(".swal2-image").on("click",function(){
                                    window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                                  }).css("cursor","pointer");
                                ivo(ST+"m").text(puntos);
                                ivo(ST+"qm").html("");
                                ivo.play("coin");
                                clearInterval(reloj);
                                ivo(ST+"r").text("00");
                                setTimeout(function(){
                                    contador+=1;
                                    ivo(ST+"robot").show();
                                    ivo(ST+"Text2 p").text("CPU");
                                    sym.getSymbol("tablero").play("p"+contador);
                                },1500);
                            }
                            sym.getSymbol("tablero").play("p"+contador);
                        });
                    }
               );
            },
            events:function(){
                var t=this;
                ivo(ST+"ok").on("click",function(){
                    stage1.reverse().timeScale(3);
                    stage2.play().timeScale(1);
                    ivo.play("clic");
                });
                
                ivo(".a").on("click",function(){
                    let id = "#"+ivo(this).attr("id")+"_";
                    stage2.reverse().timeScale(4);
                    stage3.play().timeScale(1);
                    ivo.play("clic");
                    ivo(".fotos").hide();
                    ivo(id).show();
                    t.launch();
                    control = setInterval(timer,10);
                });
                
                ivo(ST+"icon1").on("click",function(){
                    ivo(".ins").hide();
                    ivo(ST+"page_1").show();
                    ivo.play("clic");
                });
                $(ST+"icon2").addClass("animated infinite tada");
                ivo(ST+"icon2").on("click",function(){
                    $(ST+"icon2").removeClass("animated infinite tada");
                    ivo(".ins").hide();
                    ivo(ST+"page_2").show();
                    ivo.play("clic");
                });
            },
            animation:function(){
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"msg", .8,             {x:1300,opacity:0}), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"grupo", .8,           {x:1300,opacity:0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.fromTo(ST+"stage3", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage3.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.fromTo(ST+"stage4", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage4.stop();

            }
        }
 });
}
